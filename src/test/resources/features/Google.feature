Feature: Google Scenarios

  @regression @google @high
  Scenario: Google - positive scenario
    Given Open the browser and start "google" application
    When I enter "ATDD" in the seach box and click search button
    Then I should see the results containing the string:
		"""
		ATDD stands for Acceptance Test Driven Development, it is also less commonly designated as Storytest Driven Development (STDD). It is a technique used to bring customers into the test design process before coding has begun.
		"""

  @regression @google @low
  Scenario: Google - negative scenario
    Given Open the browser and start "google" application
    When I enter "ATDD" in the seach box and click search button
    Then I should see the results containing the string:
		"""
		Acceptance Test Driven Development (ATDD) is a practice in which the whole team collaboratively discusses acceptance criteria, with examples, and then distills them into a set of concrete acceptance tests before development begins.
		"""
