Feature: GitHub Scenarios

  @regression @high @signin
  Scenario: GitHub login - positive scenario
    Given Open the browser and start "github" application
    And Navigate to "signin" page
    When Login to user account with user as "test@test.com" and pass as "test123"
    Then Validate the app message as "Incorrect username or password."

  @regression @low @signin
  Scenario: GitHub login - negative scenario
    Given Open the browser and start "github" application
    And Navigate to "signin" page
    When Login to user account with user as "test@test.com" and pass as "test123"
    Then Validate the app message as "Login Successful."

  @regression @high @pricing
  Scenario: GitHub pricing - positive scenario
    Given Open the browser and start "github" application
    And Navigate to "pricing" page
    And Navigate to "plans" page
    Then Validate the PricingTitle message as "Plans for all developers"

  @regression @low @pricing
  Scenario: GitHub pricing - negative scenario
    Given Open the browser and start "github" application
    And Navigate to "pricing" page
    And Navigate to "plans" page
    Then Validate the PricingTitle message as "Do not choose any plan"
