package com.Mani.Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.Mani.Steps", "com.Mani.Utilities"},
        //tags = ("@google and @low"),
        plugin = {"pretty",
                "html:target/site/cucumber-pretty",
                "json:target/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
)

public class TestRunner extends AbstractTestNGCucumberTests {

}


// mvn clean install
// mvn clean install -Dcucumber.filter.tags="@google and @low"
// mvn clean install -DbrowserName="chrome" -DexecPlatform="native" -Dheadless=true/false

// From cucumber version 6, use as below for tags in command line
// mvn clean install -D"cucumber.filter.tags=@SecondPortal"
