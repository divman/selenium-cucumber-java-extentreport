package com.Mani.Steps;

import com.Mani.Pages.GitHub_HomePage;
import com.Mani.Pages.GitHub_PricingPage;
import com.Mani.Pages.Google_HomePage;
import com.Mani.Utilities.PropertyFileReader;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

public class Steps extends BaseSteps {

    private final Google_HomePage googleHomePage;
    private final GitHub_HomePage gitHomePage;
    private final GitHub_PricingPage gitPricingPage;

    private static String workingPage = null;

    private final Logger log = Logger.getLogger(Steps.class);
    private final PropertyFileReader propObj = new PropertyFileReader();

    public Steps() {
        googleHomePage = new Google_HomePage(browser);
        gitHomePage = new GitHub_HomePage();
        gitPricingPage = new GitHub_PricingPage();
    }

    @Given("^Open the browser and start \"([^\"]*)\" application$")
    public void Open_the_firefox_browser_and_start_google_application(String app) throws Throwable {
        Properties props = propObj.getProperty();
        log.info("Browser opened");
        if (app.equalsIgnoreCase("google")) {
            browser.get(props.getProperty("baseURL"));
            log.info("Google website launched");
            scenario.log("Google website launched with - " + props.getProperty("baseURL"));
        } else if (app.equalsIgnoreCase("github")) {
            browser.get(props.getProperty("gitURL"));
            log.info("GitHub website launched");
            scenario.log("GitHub website launched with - " + props.getProperty("gitURL"));
        } else {
            throw new Exception("Provide any base application for testing.");
        }
    }

    @When("^Login to user account with user as \"([^\"]*)\" and pass as \"([^\"]*)\"$")
    public void Login_to_user_account_with_user_as_and_pass_as(String user, String pass) throws Throwable {
        gitHomePage.loginApp(user, pass);
        log.info("Clicked on login button");
    }

    @When("^I enter \"([^\"]*)\" in the seach box and click search button$")
    public void I_enter_in_the_seach_box_and_click_search_button(String SearchText) throws Throwable {
        googleHomePage.enterSearchText(SearchText);
        log.info("Search text typed");
        googleHomePage.clickSearchButton();
        Thread.sleep(2000);
        log.info("Clicked on search button");
    }

    @Then("^I should see the results containing the string:$")
    public void I_should_see_the_results_containing_the_string(String ExpText) throws Throwable {
        googleHomePage.compareResultText(ExpText);
        log.info("Validated the text in the website and success");
    }

    /*
    @When("^Navigate to (?:signin|pricing|plans) page$")
    public void navigate_to_signin_page() throws Throwable {
        if (page.equalsIgnoreCase("signin")) {
            gitHomePage.clickSignin();
            log.info("Clicked on signin link");
        } else if (page.equalsIgnoreCase("pricing")) {
            gitPricingPage.clickPricing();
            log.info("Clicked on pricing link");
        } else {
            gitPricingPage.clickPlans();
            log.info("Clicked on plans link");
        }
    }
    */

    @When("^Navigate to \"([^\"]*)\" page$")
    public void navigate_to_signin_page(String page) throws Throwable {
        workingPage = page;
        if (page.equalsIgnoreCase("signin")) {
            gitHomePage.clickSignin();
            log.info("Clicked on signin link");
        } else if (page.equalsIgnoreCase("pricing")) {
            gitPricingPage.clickPricing();
            log.info("Clicked on pricing link");
        } else {
            gitPricingPage.clickPlans();
            log.info("Clicked on plans link");
        }
    }

    @Then("^Validate the (?:app|PricingTitle) message as \"([^\"]*)\"$")
    public void Validate_the_app_message_as(String expMsg) throws Throwable {
        if (workingPage.equalsIgnoreCase("signin")) {
            scenario.log("Text from screen : " + gitHomePage.getErrorMessage());
            assertThat(gitHomePage.getErrorMessage()).isEqualTo(expMsg);
        } else if (workingPage.equalsIgnoreCase("plans")) {
            scenario.log("Text from screen : " + gitPricingPage.getPlanHeaderText());
            assertThat(gitPricingPage.getPlanHeaderText()).isEqualTo(expMsg);
        }
    }
}
