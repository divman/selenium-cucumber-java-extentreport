package com.Mani.Pages;

import com.Mani.Steps.BaseSteps;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GitHub_PricingPage extends BaseSteps {

    @FindBy(how = How.CSS, using = "li.d-block:nth-child(6) > details:nth-child(1) > summary:nth-child(1)")
    private WebElement linkPricing;
    @FindBy(how = How.CSS, using = "p.h00-mktg")
    private WebElement txtPlanHeaderText;
    @FindBy(how = How.XPATH, using = "//a[@data-ga-click=\"(Logged out) Header, go to Pricing\"]")
    private WebElement linkPlans;

    private final WebDriverWait wait;

    public GitHub_PricingPage() {
        PageFactory.initElements(browser, this);
        wait = new WebDriverWait(browser, Duration.ofSeconds(60));
    }

    public void clickPricing() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(linkPricing));
        /*
        If you are using headless execution then the 'Mouse Action'
        sequences may not work properly and may get timeout exception.
        */
        Actions action = new Actions(browser);
        action.moveToElement(linkPricing);
        linkPricing.click();
    }

    public void clickPlans() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(linkPlans));
        linkPlans.click();
    }

    public String getPlanHeaderText() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(txtPlanHeaderText));
        return txtPlanHeaderText.getText();
    }
}

