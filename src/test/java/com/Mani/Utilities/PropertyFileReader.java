package com.Mani.Utilities;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyFileReader {

    public Properties getProperty() throws Exception {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream("src/test/resources/Environment/QA/QAURL.properties"));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        return properties;
    }

}
