package com.Mani.Utilities;

import com.Mani.Steps.BaseSteps;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import static com.Mani.Utilities.Screenshot.takeScreenshot;

public class ServiceHooks {

    @Before
    public static void setupTest(Scenario scenario) {
        BaseSteps.selectBrowser(scenario);
    }

    @After
    public static void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            //System.out.println("Scenario Status : " + scenario.getStatus());
            takeScreenshot(BaseSteps.scenario, BaseSteps.browser);
        }
        BaseSteps.browser.quit();
    }
}
